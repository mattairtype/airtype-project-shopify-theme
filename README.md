# Shopify Theme

## Installation
1. Run `npm install`.

1. Run `bower install`.


## Configuration
Copy `.env.example` into a file named `.env` in the root of the project. Fill out the variables listed in `.env`. If any variables need to be added to `.env` be sure to add the variable names to `.env.example` as well to ensure that needed variable references are stored in version controll.


## Developing
Develop in the `src` directory. Static content should be developed in `src/static` and all assets should be developed in `src/assets`.


Browser Sync is made available via Gulp and serves/watches content from `src/static` and also watches content in `src/assets`. If content from either of these directories is updated, Browser Sync will automatically refresh or reload the browser, depending on what type of content is updated.

To start Browser Sync, simply run `gulp`.


## Building
To build, simply run `gulp build`. This will copy the files from the `src` directory into `dist` while


## Deploying
To deploy, run `gulp deploy`. This will in turn run `gulp build` and then deploy the Shopify theme to the Shopify store as defined in `.env`.
