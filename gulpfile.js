var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var dotenv      = require('dotenv').config();
var clean       = require('gulp-clean');
var cleanCSS    = require('gulp-clean-css');
var fileInclude = require('gulp-file-include');
var rename      = require('gulp-rename');
var gulpShopify = require('gulp-shopify-upload');
var sequence    = require('gulp-sequence');
var sass        = require('gulp-sass');
var sourcemaps  = require('gulp-sourcemaps');
var uglify      = require('gulp-uglifyjs');

var config = require('./package.json').gulpfileConfig;

/**
 * Clean
 *
 * Clean the `tmp` and `dist` directories.
 */
gulp.task('_clean', function () {
  return gulp.src([config.paths.tmp, config.paths.dist])
    .pipe(clean({force: true}));
});

/**
 * Clean Tmp
 *
 * Clean the `tmp` directory only.
 */
gulp.task('_clean:tmp', function () {
  return gulp.src(config.paths.tmp)
    .pipe(clean({force: true}));
});

/**
 * Clone Theme
 *
 * Copy all files in `src` to `dist` except for `assets` and `static`.
 * Copy all files from `.tmp` except for `static`.
 */
gulp.task('_clone:theme', function () {
  return gulp.src([
      config.paths.src + '/**',
      '!' + config.paths.src + '/{assets,assets/**}',
      '!' + config.paths.src + '/{static,static/**}',
      config.paths.tmp + '/**',
      '!' + config.paths.tmp + '/{static,static/**}'
    ])
    .pipe(gulp.dest(config.paths.dist));
});

/**
 * Clone Bower Components
 *
 * Clone `bower_components` into the theme.
 */
gulp.task('_clone:bower_components', function () {
  return gulp.src(config.paths.bowerComponents + '/**/*')
    .pipe(gulp.dest(config.paths.dist + '/assets/libs'));
});

/**
 * Html Task
 *
 * Parses html and injects includes.
 */
gulp.task('_html', function() {
  return gulp.src(config.paths.src + '/**/*.html')
    .pipe(fileInclude({
      prefix: '@@'
    }))
    .pipe(gulp.dest(config.paths.tmp));
});

/**
 * Scripts Task
 *
 * Compile js to the `tmp` directory.
 */
gulp.task('_scripts', function() {
  return gulp.src(config.paths.src + '/assets/**/*.js')
    .pipe(uglify('app.min.js', {
      inSourceMap: true
    }))
    .pipe(gulp.dest(config.paths.tmp + '/assets/app'))
    .pipe(browserSync.stream());
});

/**
 * Serve Task
 *
 * This task will serve the contents of `src`.
 */
gulp.task('_serve', ['_html', '_scripts', '_static', '_styles'], function() {
  browserSync.init({
    server: {
      baseDir:  config.paths.tmp + '/static',
      routes: {
        "/libs": config.paths.bowerComponents,
        "/app": config.paths.tmp + '/assets/app',
      }
    }
  });

  gulp.watch(config.paths.src + '/**/*.html', ['_html']);
  gulp.watch(config.paths.src + '/**/*.js', ['_scripts']);
  gulp.watch(config.paths.src + '/**/*.scss', ['_styles']);
  gulp.watch(config.paths.src + '/**/*.+(svg|jpg|png|gif)', ['_static']);
  gulp.watch(config.paths.tmp + '/**/*').on('change', browserSync.reload);
});

/**
 * Static Files
 *
 * This task is responsible for copying all static files to
 * the `tmp` directory.
 */
gulp.task('_static',  function() {
  return gulp.src(config.paths.src + '/static/**/*')
    .pipe(gulp.dest(config.paths.tmp + '/static'));
});

/**
 * Styles Task
 *
 * Compile scss to the `tmp` directory.
 */
gulp.task('_styles', function() {
  return gulp.src(config.paths.src + '/assets/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
        outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(cleanCSS({
      compatibility: 'ie9',
      debug: true,
    }, function(details) {
        console.log(details.name + ': ' + details.stats.originalSize);
        console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(sourcemaps.write())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(config.paths.tmp + '/assets'))
    .pipe(browserSync.stream());
});

/* -------------------- */

/**
 * Default Task
 *
 * Define the default task when running `gulp`.
 */
gulp.task('default', sequence('_clean:tmp', '_html', '_serve'));

/**
 * Build Task
 *
 * Run a sequence that will build the Shopify theme.
 */
gulp.task('build', sequence('_clean', ['_html', '_scripts', '_static', '_styles'], ['_clone:bower_components', '_clone:theme'], '_clean:tmp'));

/**
 * Dump Config
 *
 * Dump the config found in `package.json`.
 */
gulp.task('dump:config', function () {
  console.log(JSON.stringify(config, 0, 2));
});

/**
 * Dump Env
 *
 * Dump environment variables, including those found in `.env`.
 */
gulp.task('dump:env', function () {
  console.log(JSON.stringify(process.env, 0, 2));
});

/**
 * Deploy Task
 *
 * Deploy the Shopify Theme
 */
gulp.task('deploy', ['build'], function() {
  return gulp.src('./dist/+(assets|layout|config|snippets|templates|locales)/**')
    .pipe(gulpShopify(
      process.env.SHOPIFY_API_KEY,
      process.env.SHOPIFY_PASSWORD,
      process.env.SHOPIFY_URL,
      process.env.SHOPIFY_THEME_ID,
      {
        "basePath": config.paths.dist
      }
    ));
});
